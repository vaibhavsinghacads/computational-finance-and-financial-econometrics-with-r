# Assign the URL to the CSV file
data_url <- "http://assets.datacamp.com/course/compfin/sbuxPrices.csv"

# Load the data frame using read.csv
sbux_df <-read.csv(data_url, header = TRUE,stringsAsFactors = FALSE)



# The sbux_df data frame is already loaded in your work space

# Check the structure of sbux_df
str(sbux_df)

# Check the first and last part of sbux_df
head(sbux_df)
tail(sbux_df)


# Get the class of the Date column of sbux_df
class(sbux_df$Date)

# The sbux_df data frame is already loaded in your work space
closing_prices <- sbux_df[,"Adj.Close", drop = FALSE]

# The sbux_df data frame is already loaded in your work space

# Find indices associated with the dates 3/1/1994 and 3/1/1995
index_1 <- which(sbux_df$Date == "3/1/1994")
index_2 <- which(sbux_df$Date == "3/1/1995")

# Extract prices between 3/1/1994 and 3/1/1995
some_prices <- sbux_df[which(sbux_df$Date == "3/1/1994"):which(sbux_df$Date == "3/1/1995"), "Adj.Close"]